variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "avail_zone" {}
variable "env_perfix" {}
variable "my_ip" {}
variable "myserver_ami" {}
variable "instance_type" {}
variable "public_key_location" {}

resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_block

    tags = {
      Name : "${var.env_perfix}-vpc"
    }
  
}

resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = aws_vpc.myapp-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone

    tags = {
      Name : "${var.env_perfix}-subnet-1"
    }
  
}

resource "aws_internet_gateway" "myapp-igw" {
    vpc_id = aws_vpc.myapp-vpc.id
    
    tags = {
      Name : "${var.env_perfix}-igw"
    }

}

# resource "aws_route_table" "myapp-route-table" {
#   vpc_id = aws_vpc.myapp-vpc.id
#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.myapp-igw.id
#   }

#   tags = {
#     Name : "${var.env_perfix}-route-table"
#   }
  
# }

# resource "aws_route_table_association" "a-rtb-subnet" {
#   subnet_id = aws_subnet.myapp-subnet-1.id
#   route_table_id = aws_route_table.myapp-route-table.id

# }

resource "aws_default_route_table" "myapp-main-rtb" {
     default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id
     route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
     }
     tags = {
       Name : "${var.env_perfix}-main-rtb"
     }
}

#Use the default securiy group
# resource "aws_default_security_group" "default-sq" {

#     vpc_id = aws_vpc.myapp-vpc.id

#     ingress {
#         from_port = 22
#         to_port = 22
#         protocol = "tcp"
#         cidr_blocks = [var.my_ip ]
#     }
#      ingress {
#         from_port = 8080
#         to_port = 8080
#         protocol = "tcp"
#         cidr_blocks = [ "0.0.0.0/0" ]
#     }

#     egress {
#         from_port = 0
#         to_port = 0
#         protocol = "-1"
#         cidr_blocks = [ "0.0.0.0/0" ]
#         prefix_list_ids = [ ]
#     }
    # tags = {
    #   Name : "${var.env_perfix}-default-sg"
    # }
  
# }
resource "aws_security_group" "myapp-sq" {
    name = "myapp-sq"
    vpc_id = aws_vpc.myapp-vpc.id

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
    
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [ "0.0.0.0/0" ]
        prefix_list_ids = []
    }

    tags = {
      Name : "${var.env_perfix}-sg"
    }
}

# resource "aws_key_pair" "server-key" {
#     key_name = "server-key"
#     #public_key = file(var.public_key_location)
#      public_key = "${file(var.public_key_location)}"
#     #public_key = var.public_key_location
    
  
# }

resource "aws_instance" "myapp-server" {
    ami = var.myserver_ami
    instance_type = var.instance_type

    subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_security_group.myapp-sq.id ]
    availability_zone = var.avail_zone


    associate_public_ip_address = true
    key_name = "myappKeyPair"
    #key_name = aws_key_pair.server-key.key_name
    

    user_data = file("entry-script.sh")
    tags = {
      Name : "${var.env_perfix}-server"
    }
}

output "dev_vpc_id" {
    value = aws_vpc.myapp-vpc.id
  
}

output "ec2_public_ip" {
  value = aws_instance.myapp-server.public_ip

}
output "dev_subnet_id" {
    value = aws_subnet.myapp-subnet-1.id
  
}

# output "dev_route_table_id" {
#     value = aws_route_table.myapp-route-table.id
  
# }

output "dev_igw_id" {
    value = aws_internet_gateway.myapp-igw.id
  
}

